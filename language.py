import languages.en_US
from languages import en_US

defaulttranslations = en_US.translations


def setLanguage(lang):
    global translations
    newlang = __import__(lang)
    translations = newlang.translations


def unsetLanguage():
    global translations
    translations = defaulttranslations


def translate(phrase):
    if phrase in translations:
        return translations[phrase]
    if phrase in defaulttranslations:
        return defaulttranslations[phrase]
    return phrase
